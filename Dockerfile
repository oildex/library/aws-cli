FROM python:3.7.4-alpine3.10

ARG AWSCLI_VERSION=1.16.225
RUN pip install awscli==$AWSCLI_VERSION
RUN apk add --no-cache --virtual .run-deps \
      groff \
      less

ARG AWS_SAM_CLI_VERSION=0.20.1
RUN apk add --no-cache --virtual .build-deps \
      gcc \
      musl-dev && \
    pip install aws-sam-cli==$AWS_SAM_CLI_VERSION && \
    apk del --no-cache .build-deps

CMD ["/usr/local/bin/aws", "--version"]

LABEL org.label-schema.url="https://aws.amazon.com/cli/"

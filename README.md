# AWS Command Line Interface

The [AWS Command Line Interface (CLI)](https://aws.amazon.com/cli/) is a unified tool to manage AWS services.

This repository extends the [python](https://hub.docker.com/_/python) image by adding the [awscli](https://pypi.org/project/awscli/) and [aws-sam-cli](https://pypi.org/project/aws-sam-cli/) modules.
